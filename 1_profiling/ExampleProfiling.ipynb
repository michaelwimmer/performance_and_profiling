{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Profiling\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Description of the physical system and the simulation\n",
    "\n",
    "We are simulating a chain of balls that are connected to their neighbors through springs:\n",
    "\n",
    "![](sketch_system.png)\n",
    "\n",
    "The positions of the $N$ balls are denoted by $\\vec{x}_i$ ($i=1, \\dots, N$), and the springs are characterized\n",
    "by a spring constant $k$ and a rest distance $d_\\mathrm{rest}$.\n",
    "\n",
    "The potential energy in this system is given as\n",
    "$$\n",
    "U = \\sum_{i=1}^{N-1} \\frac{1}{2} k \\left(\\left|\\vec{x}_i - \\vec{x}_{i+1}\\right| - d_\\mathrm{rest}\\right)^2.\n",
    "$$\n",
    "\n",
    "The force on ball $i$ from the surrounding balls is \n",
    "\n",
    "$$ F_{i} = \\sum_{j=\\{i-1, i+1\\}} -k \\left(\\left|\\vec{x}_i - \\vec{x}_j\\right| - d_\\mathrm{rest}\\right)  \\frac{\\vec{x}_i - \\vec{x}_j}{\\left|\\vec{x}_i - \\vec{x}_j\\right|}$$\n",
    "\n",
    "As mentioned above, we want to keep the two outermost balls fixed. We can achieve this by setting the forces $F_1=F_N=0$.\n",
    "With this explicit form of the force we can solve Newton's equations of motion.\n",
    "\n",
    "For given initial positions (and assuming the initial velocity is 0), this is done in the module `simulation`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from simulation import simulate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example simulation\n",
    "Let's have a look at one example we can do with our simulation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fill in the initial positions of the balls\n",
    "N = 50\n",
    "\n",
    "xs = np.arange(N+1)\n",
    "ys = np.append(xs[:40] * 10/40, (50 - xs[40:])*10/10)\n",
    "pos_init = np.stack([xs, ys], axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(pos_init[:, 0], pos_init[:, 1], marker='o')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result = simulate(pos_init, 200.0, 2000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from matplotlib import animation\n",
    "from IPython.display import HTML\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.set_xlim(( 0, 50))\n",
    "ax.set_ylim((-10, 10))\n",
    "\n",
    "line, = ax.plot([], [], lw=2, marker='o')\n",
    "\n",
    "def init():\n",
    "    line.set_data([], [])\n",
    "    return (line,)\n",
    "\n",
    "\n",
    "def animate(i):\n",
    "    line.set_data(result[i*10][:,0], result[i*10][:,1])\n",
    "    return (line,)\n",
    "\n",
    "plt.close(fig) # Don't show the empty plot \n",
    "\n",
    "# call the animator. blit=True means only re-draw the parts that have changed.\n",
    "anim = animation.FuncAnimation(fig, animate, init_func=init,\n",
    "                               frames=200, interval=20, blit=True)\n",
    "\n",
    "HTML(anim.to_jshtml())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How long does the code take?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far, the focus of our efforts has been to obtain a working code that gives a correct result - we haven't given much thought to efficiency. But that's quite ok! Correctness certainly comes first.\n",
    "\n",
    "Let us have a look how long a simulation takes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "result = simulate(pos_init, 200.0, 2000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What does that number now mean? Is it good or is it bad?\n",
    "\n",
    "It really depends on what you want to achieve. If code is fast enough so that you can achieve what you want to achieve in a for you reasonable time, then it is fine! If you are however limited by your code, it makes sense to think how you can make it more efficient. Remember that optimizing your code also takes time - you have to weigh that time against possible gains you can achieve."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the sake of this tutorial, let's assume we are in need of a more efficient code. So what should we do? Go through all of our code and try to think of more efficient expressions? In general, this is not advisable. There is a famous quote saying [\"Premature optimization is the root of all evil\"](https://softwareengineering.stackexchange.com/a/80092) - what it means is that you should first figure out which part of your code can gain the most from optimization.\n",
    "\n",
    "The current example is of course not very complex, and you might be able to tell which part is least efficient. But in more complex code this is not easy, and sometimes counter-intuitive. Fortunately, there are analysis tools, so-called \"profilers\" that can help you identify the parts of your code that take most of the time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Profiling"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using the line profiler"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(If the following command fails, you may need to install `line_profiler` by hand. In this case, execute `pip install line_profiler` in a shell.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to use the line profiler, we first need to load it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext line_profiler"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When running the line profiler, we need to specify which function(s) it should profile, by using the `-f` argument"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%lprun -f simulate simulate(pos_init, 200.0, 2000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%lprun -f simulate -f simulation.spring_force simulate(pos_init, 300.0, 3000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using the Python profiler"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%prun simulate(pos_init, 300.0, 3000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Graphical representation of profiling output"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%prun -D profile.pstat simulate(pos_init, 300.0, 3000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(If the command below does not work, you may have to install `gprof2dot` by executing `pip install gprof2dot` and `dot` by executing e.g. `conda install graphviz`)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gprof2dot -f pstats profile.pstat | dot -Tsvg -o output.svg\n",
    "from IPython.display import SVG\n",
    "SVG(filename=\"output.svg\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Summary\n",
    "\n",
    "From the profilers, we can clearly see that the code spends almost all of the time in `spring_force` - actually more than 99% of the time! This is a very clear optimization target.\n",
    "\n",
    "In the other parts of this tutorial we will consider how to optimize this function."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:root] *",
   "language": "python",
   "name": "conda-root-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
