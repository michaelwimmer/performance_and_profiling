
## Week 1

### Milestones

1. Implement dirichlet boundary conditions for the force. 
2. Simulate a chain of 50 balls.

### Planning

We discussed during the class the following division of the milestones: 

- [x] Milestone 1: Implement dirichlet boundary conditions. Jantje will start by implementing this milestone.
- [x] Milestone 2: Simulate a chain of 50 balls. Pietje will start by implementing this milestone.

We also discussed that we will do a code review on Saturday and then sit together on Monday to create the journal together. 

### Progress report

#### Milestone 1: 

- Dirichlet boundary conditions means that there is a specific value prescribed at the boundary. The milestone didn't prescribe exactly which value this should be, but as we are simulating a chain of balls so we think it makes most sense to set the  boundary values to 0. - We have implemented this by making sure in the `spring_force` function that we only calculate the forces starting from the second and going up to second to last ball, as can be seen [here](1_profiling/simulation.py#L59) in this line of code. 
- To check that this works we have in [`main.py`](1_profiling/main.py#L15-21) given random positions of 20 balls and then plotted the force on the first, last and a ball in the middle to confirm that the first and last ball always have zero force on them. The plot shows that we have indeed implemented this milestone correctly: 

![Dirichlet](1_profiling/figures/DirichletBC.png)

#### Milestone 2

- For milestone 2 we implemented a chain of 50 balls. We have done this by implementing the euler algorithm in the `simulate` function. In order to check that the simulation works we first thought about what we expect to happen during a simulation. Because the chain of balls are connected to each other by springs, we expect the distance between the balls to be not unreasonably large. Thus to check that nothing strange is happening, we first calculated the distance between each ball and the one before it, and then plotted the maximum distance found between two balls for each timestep. The exact calculation is shown in [this](1_profiling/main.py#L37) line. 

![](1_profiling/figures/chain_of_balls.png)

The plot shows that the distance between the balls is always less than 2, which is what we expect from our simulation parameters and initial conditions. 