# %%
import numpy as np
from simulation import simulate, spring_force
import matplotlib.pyplot as plt

# %%

# Fill in the initial positions of the balls
N = 50
xs = np.arange(N + 1)
ys = np.append(xs[:40] * 10 / 40, (50 - xs[40:]) * 10 / 10)
pos_init = np.stack([xs, ys], axis=1)

# %%
# To check the dirichlet boundary conditions
n_random = 100
multiple_random_pos = np.random.rand(n_random, N, 2)
multiple_f = []
for random_pos in multiple_random_pos:
    multiple_f.append(spring_force(random_pos))
multiple_f = np.array(multiple_f)
# %%
# Now plotting it:
plt.scatter(np.arange(n_random), multiple_f[:, 0, 0], label="F_x ball 0 ")
plt.scatter(np.arange(n_random), multiple_f[:, 0, 1], label="F_y ball 0 ")
plt.scatter(np.arange(n_random), multiple_f[:, N - 1, 0], label="F_x ball N-1")
plt.scatter(np.arange(n_random), multiple_f[:, N - 1, 1], label="F_y ball N-1")
plt.scatter(np.arange(n_random), multiple_f[:, 1, 0], label="F_x ball 1")
plt.legend()
plt.show()
# %%
# Simulating the system:
result = simulate(pos_init, 200.0, 2000)
# %%
# check maximum of distance between balls at each time step
result = np.array(result)
max_rel_dist = np.max(np.linalg.norm(result[:, :-1] - result[:, 1:], axis=-1), axis=-1)
plt.plot(max_rel_dist)
plt.xlabel("Time step")
plt.ylabel("Max relative distance between balls")
plt.show()

# %%
