{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Optimization - making better use of `numpy`\n",
    "\n",
    "In this tutorial we will \"numpify\" the function `spring_force` and enhance the performance in this way."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reminder: where we started from"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fill in the initial positions of the balls\n",
    "N = 50\n",
    "\n",
    "xs = np.arange(N+1)\n",
    "ys = np.append(xs[:40] * 10/40, (50 - xs[40:])*10/10)\n",
    "pos_init = np.stack([xs, ys], axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit\n",
    "simulation.simulate(pos_init, 200.0, 2000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see how much we can improve that!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The baseline: the original implementation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From our profiling, we know that almost all of the simulation time is spent in `spring_force`. We can thus focus on this specific function.\n",
    "\n",
    "We start from the original implementation, which I copy below for reference (docstring removed):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SPRING_CONSTANT = 2\n",
    "REST_DIST = 1\n",
    "\n",
    "def spring_force(pos):\n",
    "    \n",
    "    f = np.zeros_like(pos)\n",
    "    for i in range(1, len(pos)-1):\n",
    "        # force from previous ball\n",
    "        dist_vec = pos[i] - pos[i-1]\n",
    "        dist = np.sqrt(np.sum(dist_vec**2))\n",
    "        f[i] += - SPRING_CONSTANT * (dist - REST_DIST) * dist_vec/dist\n",
    "        \n",
    "        # force from the next ball\n",
    "        dist_vec = pos[i] - pos[i+1]\n",
    "        dist = np.sqrt(np.sum(dist_vec**2))\n",
    "        f[i] += - SPRING_CONSTANT * (dist - REST_DIST) * dist_vec/dist\n",
    "        \n",
    "    return f\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The orginal function serves two purposes for us:\n",
    "1. It sets the baseline of how much more efficient we can get\n",
    "2. It serves as a reference with which we can check the correctness of our new implementation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition to the function itself, we also need some input data. Here, I choose a random matrix of the right size:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "input_data = np.random.random_sample(size=(N,2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit\n",
    "spring_force(input_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initial attempt at optimization: implement numpy array operations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is clear that our best shot at optimization is to replace the `for i in range(1, len(pos)-1):` loop with numpy expressions.\n",
    "\n",
    "For every ball, we need to compute the distance vector to the previous and the next ball. This we can do easily with slicing: in this way we can subtract the array shifted by one in either direction. For example the expression\n",
    "```\n",
    "dist_vec = pos[1:-1] - pos[:-2]\n",
    "```\n",
    "gives us the distance vector to the previous ball.\n",
    "\n",
    "With this observation we can try to make a numpified version. When computing the absolute distance, we need to take care additionally that `np.sum` should only sum over the two coordinates, and not all of the array. For this we can use the `axis` argument.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def spring_force_first_try(pos):\n",
    "\n",
    "    f = np.zeros_like(pos)\n",
    "    \n",
    "    # force from previous ball\n",
    "    dist_vec = pos[1:-1] - pos[:-2]\n",
    "    dist = np.sqrt(np.sum(dist_vec**2, axis=1))\n",
    "    f[1:-1] += - SPRING_CONSTANT * (dist - REST_DIST) * dist_vec/dist\n",
    "        \n",
    "    # force from the next ball\n",
    "    dist_vec = pos[1:-1] - pos[2:]\n",
    "    dist = np.sqrt(np.sum(dist_vec**2, axis=1))\n",
    "    f[1:-1] += - SPRING_CONSTANT * (dist - REST_DIST) * dist_vec/dist\n",
    "        \n",
    "    return f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, when we run this code, we find that it *does not work*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spring_force_first_try(input_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We find that the error occurs when we try to multiply the array of distance vectors (which is a $N-2\\times 2$ matrix) with the array of absolute distances (which is a vector of length $N-2$).\n",
    "\n",
    "We know that `numpy` operations are element-wise, and indeed here numpy does not know what to do with two arrays that have different shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Background information: numpy broadcasting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When trying to do element-wise operations on arrays, `numpy` performs the operation when the two arrays have the same shape, *or* when certain other conditions are fulfilled: This is called [broadcasting](). In a nutshell, citing from the documentation:\n",
    "\n",
    ">When operating on two arrays, NumPy compares their shapes element-wise. It starts with the trailing (i.e. rightmost) dimensions and works its way left. Two dimensions are compatible when\n",
    ">\n",
    ">   1. they are equal, or\n",
    ">   2. one of them is 1\n",
    "\n",
    "When a dimension is of size 1, then numpy simply repeats the value!\n",
    "\n",
    "Let's see how this works in a simply example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = np.array([[1, 2],\n",
    "              [3, 4],\n",
    "              [5, 6]])\n",
    "B = np.array([1, 2, 3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We already know that multiplying these two does not work:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A * B"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, we can turn the vector `B` into a $3\\times 1$ matrix. You could use `np.reshape` for that, or the following syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(B[:, np.newaxis])\n",
    "print(\"has shape:\", B[:, np.newaxis].shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(`np.newaxis` actually is equal to `None`, so you might also encounter the syntax `B[:, None]`)\n",
    "\n",
    "With that we can now write"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A * B[:, np.newaxis]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To repeat what broadcasting does: The dimension that is 1 now is the number columns. To multiply with `A`, numpy thus inserts as many copies of\n",
    "this column to match the dimensions of `A`. Explicitly, the above expression is equivalent to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A * np.array([[1, 1],\n",
    "              [2, 2],\n",
    "              [3, 3]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Final version: making use of numpy broadcasting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With this knowledge, we can now arrive at the final working version of our code. In all the operations where we had a vector times a matrix before, we want to act with the entries of the vector equally on each row (which corresponds to the coordinates). This is exactly the example we saw before."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def spring_force_numpy(pos):\n",
    "    \n",
    "    f = np.zeros_like(pos)\n",
    "    \n",
    "    dist_vec = pos[1:-1] - pos[:-2]\n",
    "    dist = np.sqrt(np.sum(dist_vec**2, axis=1))\n",
    "    f[1:-1] +=  - SPRING_CONSTANT * (dist - REST_DIST)[:, np.newaxis] * dist_vec/dist[:, np.newaxis]\n",
    "    \n",
    "    dist_vec = pos[1:-1] - pos[2:]\n",
    "    dist = np.sqrt(np.sum(dist_vec**2, axis=1))\n",
    "    f[1:-1] +=  - SPRING_CONSTANT * (dist - REST_DIST)[:, np.newaxis] * dist_vec/dist[:, np.newaxis]\n",
    "        \n",
    "    return f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see if it works, and if it gives the same result as the original version!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.allclose(spring_force(input_data), spring_force_numpy(input_data))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So how much did we gain in terms of speed?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit\n",
    "spring_force_numpy(input_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Not bad, more than a factor of 20!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Putting it all together\n",
    "\n",
    "Given that `spring_force` took almost all of our simulation time, we can expect a similar speed up for our whole program. Let us try it out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import simulation_numpy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit\n",
    "simulation_numpy.simulate(pos_init, 200.0, 2000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We find a speed-up of also around a 20 (on my computer, from 3 seconds to 0.1 seconds)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
