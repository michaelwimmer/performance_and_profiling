import numpy as np

SPRING_CONSTANT = 2
REST_DIST = 1


def simulate(pos_init, time, steps):
    """
    Simulate the motion of a chain of balls with springs using a symplectic Euler method.
    
    Parameters
    ----------
    pos_init:
        Nx2 array containing the initial positions of the N balls
    time:
        simulation time
    steps:
        number of discrete simulation time steps. The size of the time step is `time/steps`
    
    Returns
    -------
    pos_per_t:
        list of arrays with positions of the N balls for every discrete time step
    """
    pos = pos_init.copy()
    vel = np.zeros_like(pos)
    
    pos_per_t = []
    dt = time/steps
    
    for t in range(steps):
        force = spring_force_numpy(pos)
        vel = vel + dt * force
        pos = pos + dt * vel
        
        pos_per_t.append(pos)
        
    return pos_per_t


def spring_force_numpy(pos):
    """
    Compute the force on each ball due to the springs connecting to the neighboring balls. The properties
    of the springs are described by the global constants SPRING_CONSTANT and REST_DIST.
    
    Parameters
    ----------
    pos:
        Nx2 array containing the positions of the N balls
        
    Returns
    -------
    force:
        Nx2 array containign the forces acting on each ball. The force on the outermost balls
        (that are held fixed) is set to zero.
    """
    f = np.zeros_like(pos)
    
    dist_vec = pos[1:-1] - pos[:-2]
    dist = np.sqrt(np.sum(dist_vec**2, axis=1))
    f[1:-1] +=  - SPRING_CONSTANT * (dist - REST_DIST)[:, np.newaxis] * dist_vec/dist[:, np.newaxis]
    
    dist_vec = pos[1:-1] - pos[2:]
    dist = np.sqrt(np.sum(dist_vec**2, axis=1))
    f[1:-1] +=  - SPRING_CONSTANT * (dist - REST_DIST)[:, np.newaxis] * dist_vec/dist[:, np.newaxis]
        
    return f