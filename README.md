Example code to show the basics of profiling and optimizing Python code

[Binder for part 1: profiling](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.kwant-project.org%2Fmichaelwimmer%2Fperformance_and_profiling.git/HEAD?filepath=1_profiling%2FExampleProfiling.ipynb)

[Binder for part 2: optimization with numpy](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.kwant-project.org%2Fmichaelwimmer%2Fperformance_and_profiling.git/HEAD?filepath=2_numpy%2FOptimizationNumpy.ipynb)
